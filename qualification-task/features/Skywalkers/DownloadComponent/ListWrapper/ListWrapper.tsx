import React from 'react';
import ListItem from './ListItem/ListItem';
import './ListWrapper.css';

interface IListWrapperProps {
  deleteItem: (name: string) => void,
  thing: [],
}

const ListWrapper = (props: IListWrapperProps) => {

  return (
    <ul className="listWrapper__wrapper">
      {props.thing.map(item => (
        <ListItem deleteItemFunc={props.deleteItem} key={item.name} {...item} />
      ))}
    </ul>
  )
};

export default ListWrapper;
