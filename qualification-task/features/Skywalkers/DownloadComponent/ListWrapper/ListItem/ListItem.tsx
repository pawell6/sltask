import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import './ListItem.css';

interface IListItemProps {
  name: string,
  height: string,
  mass: string,
  deleteItemFunc: (e: string) => void,
}

const ListItem = (
  props: IListItemProps
) => {
  return (
    <li className="listItem__wrapper">
      <div>
        <h2 className="listItem__name">
          {props.name}
        </h2>
        <p className="listItem__description">
          {"height : " + props.height}
        </p>
        <p className="listItem__description">
          {"mass : " + props.mass}
        </p>
        <Button
          variant="contained"
          color="secondary"
          onClick={() => props.deleteItemFunc(props.name)}
        >
          Delete
            </Button>
      </div>
    </li>
  )
};

ListItem.propTypes = {
  name: PropTypes.string.isRequired,
  height: PropTypes.string,
  mass: PropTypes.string,
};

ListItem.defaultProps = {
  height: 'height unknown',
  mass: 'mass unknown'
}

export default ListItem;
