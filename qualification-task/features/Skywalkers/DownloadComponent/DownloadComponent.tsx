import React from 'react';
import ListWrapper from './ListWrapper/ListWrapper';
import { bool } from 'prop-types';

class DownloadComponent extends React.Component {

  // constructor(props){
  //     super(props);
  //     this.state = {
  //         items: [],
  //         isLoaded: false,

  //     }
  // }
  private DownloadJSONAndSave() {
    fetch(this.state.URLAddress)
      .then(res => res.json())
      .then(json => {
        this.setState({
          isLoaded: true,
          items: json,
        })
      })
  }

  public state = {
    items: [],
    isLoaded: false,
    URLAddress: 'https://swapi.co/api/people/?search=Skywalker&format=json',
  }

  public ShowAllElements() {
    console.log(this.state.items.results);
  }

  public componentDidMount() {
    this.DownloadJSONAndSave();
  }

  public AddNewItem = (e: string): void => {

  }

  public DeleteItem = (e: string): void => {
    let newItems: any = this.state.items;
    for (var i = 0; i < newItems.results.length; i++) {
      if (newItems.results[i].name === e) {
        newItems.results.splice(i, 1);
      }
    }
    console.log("nowa tab dl:" + newItems.results.length);
    this.setState({
      items: newItems,
    });
  }

  public render() {
    var { isLoaded, items } = this.state;

    if (!isLoaded) {
      return <div>Loading...</div>;
    }
    else {
      return (
        <ListWrapper deleteItem={this.DeleteItem} thing={this.state.items.results} />
      )
    }
  }
}

export default DownloadComponent;
